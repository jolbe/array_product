const productBruteForce = array => {
  let result = [];

  for (let i = 0; i < array.length; i++) {
    let curr = 1;
    for (let j = 0; j < array.length; j++) {
      if (i != j) {
        curr = curr * array[j];
      }
    }

    result.push(curr);
    curr = 1;
  }
  return result;
};

const productOptimized = array => {
    let result = [];

    let left = 1;
    result.push(left)
    for (let i = 1; i < array.length; i++) {
        left *= array[i-1]
        result.push(left);

    }
    
    console.log(array)

    console.log('=== l result: ', result);

    right = 1;
    for (let i = array.length - 1; i > 0; i--) {
        right *= array[i];
        result[i-1] *= right;
    }

    console.log('=== r result: ', result);


    return result;
}

console.log(productOptimized([3,4,5,6]));

module.exports = {
  productBruteForce,
  productOptimized
};

/*
[1,2,3,4,5]

0 => 1
1 => 
*/