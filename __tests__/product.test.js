const {productBruteForce, productOptimized} = require('../src/product');

describe('Product function', () => {
    const i1 = [1, 2, 3, 4, 5];
    const o1 = [120, 60, 40, 30, 24];

    const i2 = [3, 4, 5, 6];
    const o2 = [120, 90, 72, 60];

    it('should calculate product of the array', () => {
        expect(productBruteForce(i1)).toEqual(o1);
        expect(productBruteForce(i2)).toEqual(o2);
        
    })
    
    it("should calculate product of the array with different numbers", () => {
        expect(productOptimized(i1)).toEqual(o1);
        expect(productOptimized(i2)).toEqual(o2);
    });

    it("should pass examples from the internet", () => {
        const input = [3, 6, 4, 8, 9];
        const output = [1728, 864, 1296, 648, 576];
        expect(productBruteForce(input)).toEqual(output);
        expect(productOptimized(input)).toEqual(output);
    })
})